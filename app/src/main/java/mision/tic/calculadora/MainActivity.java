package mision.tic.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnSumar,btnRestar;
    private TextView txtResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //relacion
        btnSumar = findViewById(R.id.btnSumar);
        btnRestar = findViewById(R.id.btnRestar);
        txtResultado = findViewById(R.id.txtResultado);


    }

    public void btnSumar(View view){
        int resulActual = Integer.parseInt(txtResultado.getText().toString());
        int res = resulActual + 1;
        txtResultado.setText(String.valueOf(res));
    }

    public void btnRestar(View view){
        int resulActual = Integer.parseInt(txtResultado.getText().toString());
        int res = resulActual - 1;
        txtResultado.setText(String.valueOf(res));
    }

}